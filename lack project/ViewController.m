//
//  ViewController.m
//  lack project
//
//  Created by Click Labs135 on 10/26/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    CLLocationManager *manager;
}


@property (strong, nonatomic) IBOutlet UILabel *latitudeLabel;
@property (strong, nonatomic) IBOutlet UITextField *latitudeTextField;
@property (strong, nonatomic) IBOutlet UILabel *longitudeLabel;
@property (strong, nonatomic) IBOutlet UITextField *longitudeTextField;

@property (strong, nonatomic) IBOutlet UIButton *getLocation;
@end

@implementation ViewController
@synthesize latitudeLabel;
@synthesize latitudeTextField;
@synthesize longitudeLabel;
@synthesize longitudeTextField;
@synthesize getLocation;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    manager=[[CLLocationManager alloc]init];
    manager.delegate=self;
    manager.desiredAccuracy=kCLLocationAccuracyBest;
    [manager requestAlwaysAuthorization];
}
- (IBAction)getLocationPressed:(id)sender {
    [manager startUpdatingLocation];
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        longitudeTextField.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        latitudeTextField.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
